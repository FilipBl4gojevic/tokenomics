# The "gimbal token"

## Project Goals
1. Elucidate the shared values of the Gimbalabs community
2. Design incentives that increase the likelihood that our community maintains those values
3. Talking about money is a responsibility to be taken seriously. To the extent possible, create a safe place for experimentation and learning as much as possible.
4. Develop the "tokenomics" expertise of everyone who participates in a way that can be applied in communities beyond Gimbalabs.
5. **Create a utility token and token policy that best serves everyone in the Gimbalabs community, while supporting the Gimbalabs vision and maximizing the likelihood that our mission is carried out.**

## Gimbalabs Mission and Vision
Our mission is to mobilize everyone in the Cardano community by creating tools and real-world use cases that ignite the public imagination and facilitate adoption.

We envision a world where as many people as possible are empowered to solve problems using the Cardano protocol.

## Getting Started
- Initialize a wiki page to collect ideas + research
- Review other wiki pages
- First meeting and meeting cadence?
- Discord channel: _Current Projects -> #gimbal-token_
